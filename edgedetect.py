import cv2
import numpy as np
from PIL import Image, ImageChops, ImageEnhance
from io import BytesIO
import subprocess as sp
from joblib import Parallel, delayed 
import multiprocessing
from time import perf_counter


######################################################
input_file = 'video sample.mp4'
output_file = 'video sample - EdgeDetect+ELA.mp4'
kbps = '20000k'
output_resolution_factor = 1
######################################################


cap = cv2.VideoCapture(input_file)
if (cap.isOpened()== False): 
  print("Error opening video stream or file")

frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
fps = str(cap.get(cv2.CAP_PROP_FPS))

num_cores = multiprocessing.cpu_count()

print ("Starting ...")

ffmpeg = 'ffmpeg'
if output_resolution_factor >1 or output_resolution_factor<=0:
    output_resolution_factor = 1
dimension = '{}x{}'.format(int(frame_width*output_resolution_factor), int(frame_height*output_resolution_factor))

fgbg = cv2.createBackgroundSubtractorMOG2(
    history=10,
    varThreshold=2,
    detectShadows=False)

command = [ffmpeg,
        '-y',
        '-f', 'rawvideo',
        '-vcodec','rawvideo',
        '-s', dimension,
        '-pix_fmt', 'bgr24',
        '-r', fps,
        '-i', '-',
        '-an',
        '-vcodec', 'libx264',
        '-b:v', kbps,
		'-q:v', '0',
		'-crf','18',
        output_file ]

def colorize(i):
	for j in range(ela_im.size[1]):
		if pixels[i,j] == (255, 255, 255):
			pixels[i,j] = (0, 0 ,255)

proc = sp.Popen(command, bufsize=0, stdin=sp.PIPE, close_fds=True)

start_time = perf_counter()

while(cap.isOpened()):
  ret, frame = cap.read()
  if ret == True:

    fout = proc.stdin
  
    frame = cv2.resize(frame, (int(frame_width*output_resolution_factor), int(frame_height*output_resolution_factor))) 

    resaved = BytesIO()

    im = Image.fromarray(frame)
    im.save(resaved, "jpeg", quality=95)
    resaved_im = Image.open(resaved)

    ela_im = ImageChops.difference(im, resaved_im)
    extrema = ela_im.getextrema()
    max_diff = max([ex[1] for ex in extrema])
    if max_diff != 0:
        scale = 6000.0/max_diff
    else:
        scale = 6000.0

    ela_im = ImageEnhance.Brightness(ela_im).enhance(scale)
    converted = np.where(np.array(ela_im) == 255, 255, 0)
    ela_im = Image.fromarray(converted.astype('uint8'))

    gray = ela_im.convert('L')
    flat = gray.convert('RGB')

    bw = np.asarray(flat).copy()
    bw[bw < 230] = 0    
    bw[bw >= 230] = 255
    ela_im = Image.fromarray(bw)
	
    pixels = ela_im.load()
	
    i = 0
    resulted = Parallel(n_jobs=int(int(num_cores)-1), require='sharedmem')(delayed(colorize)(i) for i in range(ela_im.size[0]))	

	
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    edges_foreground = cv2.bilateralFilter(gray, 9, 75, 75)
    foreground = fgbg.apply(edges_foreground)
    
    kernel = np.ones((50,50),np.uint8)
    foreground = cv2.morphologyEx(foreground, cv2.MORPH_CLOSE, kernel)

    gray_filtered = cv2.bilateralFilter(gray, 7, 50, 50)

    edges = cv2.Canny(gray, 20, 30)
    edges_filtered = cv2.Canny(gray_filtered, 60, 120)
    edges_foreground = cv2.bilateralFilter(gray, 9, 75, 75)
    edges_filtered_2 = cv2.Canny(edges_foreground, 60, 120)
 
    edges_high_thresh = cv2.Canny(gray, 55, 75)

    cropped = (foreground // 255) * edges_filtered_2

    trichannels = cv2.cvtColor(edges_high_thresh, cv2.COLOR_GRAY2BGR)

    overlayed = cv2.addWeighted(trichannels,0.5,frame,0.8,0,0)
    overlayed = cv2.addWeighted(np.array(ela_im),1,overlayed,0.5,0,0)

    # Uncoment to display video
    # cv2.imshow('Frame', overlayed)
	
    fout.write(overlayed.tostring())

    if cv2.waitKey(25) & 0xFF == ord('q'):
      break

  else: 
    break

cap.release()
proc.stdin.close()
cv2.destroyAllWindows()
fout.close()
proc.wait()
if proc.returncode !=0: raise subprocess.CalledProcessError(proc.returncode,command)

elapsed_time = perf_counter() - start_time
print("\nDone in " + str(elapsed_time) + "s !")
